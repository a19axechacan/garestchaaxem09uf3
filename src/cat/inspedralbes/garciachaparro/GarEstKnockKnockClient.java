package cat.inspedralbes.garciachaparro;



import java.io.*;
import java.net.*;

public class GarEstKnockKnockClient {
    public static void main(String[] args) throws IOException {
        
        if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        Socket kkSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
        	//1.Open a client socket.
        	kkSocket = new Socket(hostName, portNumber);
        	//3.Open the output stream to the client socket.	
        	out = new PrintWriter(kkSocket.getOutputStream(), true);
        	//2.Open the input stream of the client socket
            in = new BufferedReader(
                new InputStreamReader(kkSocket.getInputStream()));
        
            BufferedReader stdIn =
                new BufferedReader(new InputStreamReader(System.in));
            String fromServer;
            String fromUser;

            while ((fromServer = in.readLine()) != null) {
                System.out.println("Server: " + fromServer);
                if (fromServer.equals("Bye."))
                    break;
                //4.Read from the client socket input stream.
                fromUser = stdIn.readLine();
                if (fromUser != null) {
                	//5.Write to the client socket output stream. 
                    System.out.println("Client: " + fromUser);
                    out.println(fromUser);
                	}
            	}
          //12. Exception thrown to indicate that the IP address of a host (the server) could not be determined.
            }catch (UnknownHostException e) {
            	System.err.println("Don't know about host " + hostName);
            	System.exit(1);
            
            } catch (IOException e) {
            	System.err.println("Couldn't get I/O for the connection to " +
            	hostName);
            	System.exit(1);
        }finally {
        	//6.Close the input stream of the client socket.
        	in.close();
        	//7.Close the output stream of the client socket. 
        	out.close();
        	//8.Close the client socket. 
        	kkSocket.close();
        }
    }
}
