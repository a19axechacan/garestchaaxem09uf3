package cat.inspedralbes.garciachaparro;



import java.io.*;
import java.net.*;

public class ChaAxeEchoClient {
    public static void main(String[] args) throws IOException {
       
        if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        Socket echoSocket = null ;
        PrintWriter out = null ;
        BufferedReader in = null;
        BufferedReader stdIn=null;
        try {
        	//1.-Open a client socket.
             echoSocket = new Socket(hostName, portNumber);
        		
        	 System.out.println("IP Client(my IP): "+ echoSocket.getLocalAddress());
        	 System.out.println("Port Client(my port): "+ echoSocket.getLocalPort());
        	 System.out.println("IP of the server: "+ echoSocket.getInetAddress());
        	 System.out.println("Server port: "+ echoSocket.getPort());
             		
        	//3.-Open the output stream to the client socket
             out =
                new PrintWriter(echoSocket.getOutputStream(), true);
        	//2.-Open the input stream of the client socket
             in =
                new BufferedReader(
                    new InputStreamReader(echoSocket.getInputStream()));
            //4.-Read from the client socket input stream
             stdIn =
                new BufferedReader(
                    new InputStreamReader(System.in));
    
            String userInput;
            
            //5.-Write to the client socket output stream. 
            while ((userInput = stdIn.readLine()) != null) {
            	
                out.println(userInput);
                System.out.println("echo: " + in.readLine());
            }
        } catch (UnknownHostException e) {
        	//12.-Exception thrown to indicate that the IP address of a host (the server) could not be determined.
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        }finally {
        	//6.Close the input stream of the client socket
        	in.close();
        	stdIn.close();
        	//7.-Close the output stream of the client socket
        	out.close();
        	//8.-Close the client socket
        	echoSocket.close();
        }
    }
}
