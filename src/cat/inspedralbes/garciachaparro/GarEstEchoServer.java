package cat.inspedralbes.garciachaparro;


import java.net.*;
import java.io.*;

public class GarEstEchoServer {
    public static void main(String[] args) throws IOException {
        
        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        ServerSocket serverSocket = null;
        Socket clientSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        
        try {
        		//9.Open the server socket to listen on port [indicate the port].
        		serverSocket =
                new ServerSocket(Integer.parseInt(args[0]));
        		//1.Open a client socket.
        		
            	clientSocket = serverSocket.accept();  
            
        		System.out.println("Number of the Server port: "+ clientSocket.getLocalPort());
        		System.out.println("IP of the Server: "+ clientSocket.getLocalSocketAddress());
        		
        		System.out.println("Number of the Client port: "+ clientSocket.getPort());
        		System.out.println("IP of the Client: " + clientSocket.getInetAddress());
        		
        		out =
        		//3.Open the output stream to the client socket. 
                new PrintWriter(clientSocket.getOutputStream(), true);  
	           //2.Open the input stream of the client socket
	            in = new BufferedReader(
	                new InputStreamReader(clientSocket.getInputStream()));
		        {
		        } String inputLine;
		        //4.Read from the client socket input stream.
		            while ((inputLine = in.readLine()) != null) { //10. Server processing
		        //5.Write to the client socket output stream.         
		            	out.println(inputLine);
		            }
		        } catch (IOException e) {
		            System.out.println("Exception caught when trying to listen on port "
		                + portNumber + " or listening for a connection");
		            System.out.println(e.getMessage());
		        }finally {
		        	//6.Close the input stream of the client socket.
		        	in.close();
		        	//7.Close the output stream of the client socket.
		        	out.close();
		        	//8.Close the client socket.
		        	clientSocket.close();
		        	//11. Close the server socket.
		        	serverSocket.close();
		        }
		    }
		}
