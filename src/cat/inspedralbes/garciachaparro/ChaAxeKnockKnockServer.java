package cat.inspedralbes.garciachaparro;

import java.net.*;
import java.io.*;

public class ChaAxeKnockKnockServer {
    public static void main(String[] args) throws IOException {
        
        if (args.length != 1) {
            System.err.println("Usage: java KnockKnockServer <port number>");
            System.exit(1);
        }

        int portNumber = Integer.parseInt(args[0]);
        PrintWriter out=null;
        BufferedReader in=null;
        ServerSocket serverSocket=null;
        Socket clientSocket =null;
        
        try { 
        	
        	//9. Open the server socket to listen on port [indicate the port].
             serverSocket = new ServerSocket(portNumber);
           //1.Open a client socket.
             clientSocket = serverSocket.accept();
            //3.Open the output stream to the client socket.
             out =
                new PrintWriter(clientSocket.getOutputStream(), true);
            //2.Open the input stream of the client socket.
             in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
        
        
            String inputLine, outputLine;
            
            // Initiate conversation with client
          
          
            
            ChaAxeKnockKnockProtocol kkp = new ChaAxeKnockKnockProtocol();
            outputLine = kkp.processInput(null);
            //5.Write to the client socket output stream.
            out.println(outputLine);
//4.Read from the client socket input stream
            while ((inputLine = in.readLine()) != null) {
            	  //10. Server processing.
                outputLine = kkp.processInput(inputLine);
                out.println(outputLine);
                if (outputLine.equals("Bye."))
                    break;
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }finally {
        	//6.Close the input stream of the client socket. 
        	in.close();
        	//7.Close the output stream of the client socket. 
        	out.close();
        	//8.Close the client socket.
        	clientSocket.close();
        	//11Close the server socket
        	serverSocket.close();
        }
    }
}
